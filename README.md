# codercom-intellij-idea-c

## Docker Image
`docker push ziyibear/codercom-intellij-idea-c:latest`

## Source
https://gitlab.com/ziyi-bear/codercom-intellij-idea-c.git

## Reference
* https://blog.jetbrains.com/blog/2021/03/11/projector-is-out/#DockerImages
* https://github.com/JetBrains/projector-docker

## License
For open source projects, say how it is licensed.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
